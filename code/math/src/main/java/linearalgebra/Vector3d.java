package linearalgebra;

/**
 * Vector3d class is to calculate and store vector information
 * and obtains calculations.
 * @author Amy nguyen
 * @version 2/10/2023
 */
public class Vector3d{
    private double x;
    private double y;
    private double z;

    /**
     * This is the constructor of the the class
     * @param x  - A value
     * @param y - A vector value
     * @param z - A vector value
     */
    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * A get method for x
     * @return - returns the value of this.x
     */
    public double getX(){
        return this.x;
    }

    /**
     * A get method for value y
     * @return - returns value of this.y
     */
    public double getY(){
        return this.y;
    }

    /**
     * A get method for z
     * @return - returns value for this.z
     */
    public double getZ(){
        return this.z;
    }
    /**
     * A method to calculate the magnitude with the object's values
     * @return - returns the magnitude.
     */
    public double magnitude(){
        double magnitude = Math.sqrt(x*x + y*y + z*z);
        return magnitude;
    }
    /**
     * A method to return the product of two dots 
     * @return - returns the value of dot, after calculations are done.
     */
    public double dotProduct(Vector3d v){
        double dot = (this.x * v.getX()) + (this.y*v.getY()) + (this.z * v.getZ());  
        return dot;
    }
    /**
     * A method to return the sum of two vectors added together
     * @return - returns the value after calculations are done.
     */
    public Vector3d add(Vector3d inVector){
        double newX = this.x + inVector.getX();
        double newY = this.y + inVector.getY();
        double newZ = this.z + inVector.getZ(); 
        Vector3d v = new Vector3d(newX, newY, newZ);

        return v;
    }
} 
